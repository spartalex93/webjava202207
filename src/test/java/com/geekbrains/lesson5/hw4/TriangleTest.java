package com.geekbrains.lesson5.hw4;

import com.geekbrains.hw4.TriangleArea;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TriangleTest {
    @Test
    void triangleTest() throws Exception {
        Assertions.assertEquals(TriangleArea.calculateArea(2, 2, 2), 1.73205, 0.001);
    }

    @Test
    void triangleNegativeTest() {
        Assertions.assertThrows(Exception.class, () -> TriangleArea.calculateArea(-2, 2, 2));
    }
}
